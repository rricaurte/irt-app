from alembic import op
import sqlalchemy as sa

revision = 'migration_4'
down_revision = 'migration_3'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('incident_systems',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('incident_id', sa.Integer(), nullable=False),
                    sa.Column('system_id', sa.Integer(), nullable=False),
                    sa.PrimaryKeyConstraint('id'),
                    sa.ForeignKeyConstraint(('system_id',), ['irt_system.id'], use_alter=True, ondelete='CASCADE'),
                    sa.ForeignKeyConstraint(('incident_id',), ['incident.id'], use_alter=True, ondelete='CASCADE'),
                    )
    op.add_column('incident', sa.Column('distributions_lists', sa.String(length=255), nullable=False))


def downgrade():
    op.drop_table('incident_systems')
    op.drop_column('incident', 'distributions_lists')