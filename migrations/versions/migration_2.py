from alembic import op
import sqlalchemy as sa

revision = 'migration_2'
down_revision = 'migration_1'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('irt_system',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('last_updated_at', sa.DateTime(), nullable=False),
    sa.Column('deleted_at', sa.DateTime(), nullable=True),
    sa.Column('name', sa.String(length=80), nullable=False),
    sa.Column('description', sa.String(length=120), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name'),
    )


def downgrade():
    op.drop_table('irt_system')
