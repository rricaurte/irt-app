from alembic import op
import sqlalchemy as sa

revision = 'migration_3'
down_revision = 'migration_2'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('distribution_list',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('last_updated_at', sa.DateTime(), nullable=False),
    sa.Column('deleted_at', sa.DateTime(), nullable=True),
    sa.Column('email', sa.String(length=80), nullable=False),
    sa.Column('description', sa.String(length=120), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('email'),
    )
    op.create_table('suggested_dl',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('system_id', sa.Integer(), nullable=False),
    sa.Column('dl_id', sa.Integer(), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.ForeignKeyConstraint(('system_id',), ['irt_system.id'], use_alter=True, ondelete='CASCADE'),
    sa.ForeignKeyConstraint(('dl_id', ), ['distribution_list.id'], use_alter=True, ondelete='CASCADE'),
    )


def downgrade():
    op.drop_table('distribution_list')
    op.drop_table('suggested_dl')
