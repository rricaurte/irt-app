from alembic import op
import sqlalchemy as sa

revision = 'migration_5'
down_revision = 'migration_4'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('incident_update',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('content', sa.String(255), unique=False, nullable=False),
                    sa.Column('number', sa.Integer(), unique=False, nullable=False),
                    sa.Column('incident_id', sa.Integer(), nullable=False),
                    sa.PrimaryKeyConstraint('id'),
                    sa.ForeignKeyConstraint(('incident_id',), ['incident.id'], use_alter=True, ondelete='CASCADE')
                    )


def downgrade():
    op.drop_table('update')
