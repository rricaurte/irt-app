from flask import jsonify, request, render_template, redirect, url_for, flash
from models.incident_template import *
from models.irt_system import *
import settings
from models.incident_update import *


def get():
    page = int(request.args.get("page")) if request.args.get("page") else 1
    per_page = int(request.args.get("items_per_page")) if request.args.get("items_per_page") else settings.ITEMS_PER_PAGE
    all_templates = IncidentTemplate.query.order_by(IncidentTemplate.id.desc()).filter_by(deleted_at=None).paginate(page, per_page=per_page)
    return render_template(
        "admin/views/templates.html",
        templates=all_templates,
        title="Templates",
        templates_active=True)


def put(id):
    template_data = {}
    current_template = None
    try:
        current_template = IncidentTemplate.query.filter_by(id=id).first()
        template_data = _read_template_data_from_request()
        current_template.name = template_data["name"]
        current_template.subject = template_data["subject"]
        current_template.description = template_data["description"]
        current_template.severity = int(template_data["severity"]) if template_data["severity"].isdigit() else -1
        current_template.systems = template_data["systems"]
        _load_optional_fields(current_template, template_data)
        db.session.commit()

        flash(("Template edited", "Template edited successfully"), "success")
        return redirect(url_for("templates"))
    except Exception as e:
        flash(("Error", e), "error")
        if current_template is not None:
            template_data["id"] = id
        return _render_template_for_template(template_data, True)


def post():
    template_data = {}
    try:
        template_data = _read_template_data_from_request()
        new_template = IncidentTemplate(
            name=template_data["name"],
            subject=template_data["subject"],
            description=template_data["description"],
            severity=int(template_data["severity"]) if template_data["severity"].isdigit() else -1
        )
        new_template.systems = template_data["systems"]
        _load_optional_fields(new_template, template_data)
        db.session.add(new_template)
        db.session.commit()

        flash(("Template created", "Template created successfully"), "success")
        return redirect(url_for("templates"))
    except Exception as e:
        flash(("Error", e), "error")
        return _render_template_for_template(template_data, False)


def delete(id):
    try:
        IncidentTemplate.query.filter_by(id=id).first().soft_delete()
        db.session.commit()

        flash(("Template deleted", "Template deleted successfully"), "success")
    except Exception as e:
        flash(("Error", str(e)), "error")
    return jsonify({"message": "ok"})


def show_edit(id):
    current_template = IncidentTemplate.query.filter_by(id=id).first()
    template_data = current_template.__dict__
    template_data["systems"] = current_template.systems
    return _render_template_for_template(template_data, True)


def show_create():
    return _render_template_for_template({}, edit_flag=False)


def _read_template_data_from_request():
    request_dict = request.form.to_dict(flat=False)
    template_data = {k.replace("[]", ""): request_dict[k][0] if len(request_dict[k])
                     <= 1 and k[-2:] != "[]" else request_dict[k] for k in request_dict}
    if "systems" in template_data:
        template_data["systems"] = IrtSystem.query.filter(IrtSystem.id.in_([int(i) for i in template_data["systems"]])).all()
    return template_data


def _render_template_for_template(template_data, edit_flag):
    all_systems = IrtSystem.query.filter_by(deleted_at=None).all()
    if edit_flag and "systems" in template_data:
        for sys in template_data["systems"]:
            if sys.deleted_at is not None:
                all_systems.append(sys)
    if "systems" in template_data:
        template_data["systems"] = [sys.__dict__ for sys in template_data["systems"]]

    return render_template(
        "admin/views/new_template.html",
        template_data=template_data,
        title="Templates",
        templates_active=True,
        edit_flag=edit_flag,
        incidents_severities=IncidentTemplate.ALL_SEVERITIES,
        systems=[sys.__dict__ for sys in all_systems],
    )


def _load_optional_fields(template, template_data):
    optional_fields = ["manager", "teams_working", "affected_customers_units", "distributions_lists", "cause"]
    for field in optional_fields:
        setattr(template, field, template_data[field] if field in template_data else "")