from flask import jsonify, request, render_template, redirect, url_for, flash
from models.irt_system import *
from models.distribution_list import *
import settings


def get():
    page = int(request.args.get("page")) if request.args.get("page") else 1
    per_page = int(request.args.get("items_per_page")) if request.args.get("items_per_page") else settings.ITEMS_PER_PAGE
    all_systems = IrtSystem.query.filter_by(deleted_at=None).order_by(IrtSystem.name.asc()).paginate(page, per_page=per_page)
    return render_template(
        'admin/views/systems.html',
        systems=all_systems,
        title='Systems',
        systems_active=True)


def get_dls(id):
    current_system = IrtSystem.query.filter_by(id=id, deleted_at=None).first()
    systems_valid_dls = filter(lambda dl: dl.deleted_at is None, current_system.dls)
    return jsonify([dl.email for dl in systems_valid_dls])


def put(id):
    current_system = None
    request_dict = request.form.to_dict(flat=False)
    system_data = {k.replace("[]", ""): request_dict[k][0] if len(request_dict[k])
                   <= 1 else request_dict[k] for k in request_dict}
    system_dls = []
    try:
        if "dls" not in system_data: raise Exception("No DLs provided for this system")
        current_system = IrtSystem.query.filter_by(id=id, deleted_at=None).first()
        for dl_id in system_data['dls']:
            system_dls.append(DistributionList.query.filter_by(id=int(dl_id)).first())
        current_system.dls = system_dls
        current_system.name = system_data['name']
        current_system.description = system_data['description']
        db.session.commit()

        flash(("System edited", "System edited successfully"), "success")
        return redirect(url_for('systems'))
    except Exception as e:
        flash(("Error", e), "error")
        if current_system is not None:
            system_data["id"] = current_system.id
            system_data["dls"] = system_dls
        return render_template_for_system(system_data, True)


def post():
    request_dict = request.form.to_dict(flat=False)
    system_data = {k.replace("[]", ""): request_dict[k][0] if len(request_dict[k]) <=
                   1 and k[-2:] != "[]" else request_dict[k] for k in request_dict}
    system_dls = []
    try:
        if "dls" not in system_data: raise Exception("No DLs provided for this system")
        for dl_id in system_data["dls"]:
            system_dls.append(DistributionList.query.filter_by(id=int(dl_id)).first())
        new_system = IrtSystem(name=system_data["name"], description=system_data["description"])
        new_system.dls = system_dls
        db.session.add(new_system)
        db.session.commit()

        flash(("System created", "System created successfully"), "success")
        return redirect(url_for('systems'))
    except Exception as e:
        flash(("Error", e), "error")
        return render_template_for_system(system_data, False)


def delete(id):
    try:
        IrtSystem.query.filter_by(id=id).first().soft_delete()
        db.session.commit()
        flash(("System deleted", "System deleted successfully"), "success")
    except Exception as e:
        flash(("Error", str(e)), "error")
    return jsonify({"message": "ok"})


def show_edit(id):
    current_system = IrtSystem.query.filter_by(id=id, deleted_at=None).first()
    return render_template_for_system(current_system.__dict__, True)


def show_create():
    return render_template_for_system({}, edit_flag=False)


def render_template_for_system(system_data, edit_flag):
    all_dls = DistributionList.query.filter_by(deleted_at=None).all()
    if edit_flag and "dls" in system_data:
        for dl in system_data["dls"]:
            if dl.deleted_at is not None:
                all_dls.append(dl)
    return render_template(
        'admin/views/new_system.html',
        system_data=system_data,
        distribution_lists=all_dls,
        title='System',
        edit_flag=edit_flag)
