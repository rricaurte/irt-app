from flask import jsonify, request, render_template, redirect, url_for, flash
from models.incident_update import *
from models.incident import *
from models import mailer
from models import pyjira


def get(inci_id):
    incident_updates = IncidentUpdate.query.filter_by(incident_id=inci_id)
    return jsonify([{"number": update.number, "content": update.content} for update in incident_updates])


def post(inci_id):
    request_dict = request.form.to_dict(flat=False)
    update_data = {}
    try:
        update_data = {k.replace("[]", ""): request_dict[k][0] if len(request_dict[k])
                       <= 1 and k[-2:] != "[]" else request_dict[k] for k in request_dict}
        current_incident = Incident.query.filter_by(id=inci_id).first()
        # TODO: Check if this is really neccessary
        new_update_number = IncidentUpdate.query.filter_by(incident_id=inci_id).count() + 1
        new_update = IncidentUpdate(content=update_data["content"], incident_id=current_incident.id, number=new_update_number)
        db.session.add(new_update)

        if "close_inci" in update_data and update_data["close_inci"] == "on":
            current_incident.close_inci(datetime.datetime.strptime(update_data["closure_datetime"], '%Y/%m/%d %H:%M'))

        _save_jira_comment(current_incident, new_update)
        if "send_mail" in update_data and update_data["send_mail"] == "on":
            _send_incident_email(current_incident)

        db.session.commit()
        flash(("Update added", "Update added successfully"), "success")
    except Exception as e:
        flash(("Error", str(e)), "error")
    return redirect(url_for('incidents'))


def current_state(inci_id):
    try:
        current_incident = Incident.query.filter_by(id=inci_id).first()
        mailer.send_email(current_incident)
        return jsonify({"message": "Incident data sent"}), 200
    except Exception as e:
        return jsonify({"message": "Incident data could not be sent"}), 408


def _send_incident_email(incident):
    try:
        mailer.send_email(incident)
    except mailer.MailerException as e:
        flash(("Error", str(e)), "error")
    except Exception as e:
        print("Error:" + str(e))
        flash(("Error", "Mailer couldn't be reached"), "error")


def _save_jira_comment(incident, update):
    try:
        if not incident.is_on_jira():
            incident.code = pyjira.create_ticket(incident)
            incident_updates = sorted(IncidentUpdate.query.filter_by(incident_id=incident.id), key=lambda x: x.number)
            for update in incident_updates:
                pyjira.comment_ticket(incident.code, _update_to_comment(update))
        else:
            pyjira.comment_ticket(incident.code, _update_to_comment(update))
            pyjira.update_ticket(incident)
    except pyjira.JiraException as e:
        flash(("Error", str(e)), "error")
    except Exception as e:
        print("Error:" + str(e))
        flash(("Error", "Jira couldn't be reached"), "error")


def _update_to_comment(update):
    return "Update #" + str(update.number) + ":\n" + update.content
