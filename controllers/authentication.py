from flask import request, redirect, url_for, render_template, flash, session
import requests
# import redis
import settings


def login():
    try:
        res = requests.post(
            settings.LDAP_ENDPOINT,
            json={"username": request.form["username"], "password": request.form["password"]},
            headers=settings.LDAP_HEADERS,
            timeout=settings.LDAP_TIMEOUT)
        if(res.status_code != 200):
            raise AssertionError("Invalid credentials")
        flash(("Login successful", "Welcome " + request.form["username"]), "success")
        session.permanent = True
        session["irt_reporter"] = request.form["username"]
        return redirect(url_for("incidents"))
    except Exception as e:
        flash(("Error", str(e)), "error")
        return redirect(url_for("login"))


def show_login():
    return render_template("login.html")


def logout():
    session.pop("irt_reporter", None)
    return redirect(url_for("login"))
