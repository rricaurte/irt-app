from flask import jsonify, request, render_template
from models.user import *


def get():
    all_users = User.query.all()
    resp = {"users": []}
    for u in all_users:
        resp["users"].append({"username": u.username, "email": u.email})
    return jsonify(resp), 200


def post():
    body = request.json
    username = body['username']
    email = body['email']
    db.session.add(User(username=username, email=email))
    db.session.commit()
    return jsonify({"message": "User added"}), 201
