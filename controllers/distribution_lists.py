from flask import jsonify, request, render_template, redirect, url_for, flash
from models.distribution_list import *
import settings


def get():
    page = int(request.args.get("page")) if request.args.get("page") else 1
    per_page = int(request.args.get("items_per_page")) if request.args.get("items_per_page") else settings.ITEMS_PER_PAGE
    all_dls = DistributionList.query.filter_by(deleted_at=None).order_by(DistributionList.email.desc()).paginate(page, per_page=per_page)
    return render_template(
        'admin/views/distribution_lists.html',
        dls=all_dls,
        title='Distribution Lists',
        dls_active=True)


def put(id):
    current_dl = None
    request_dict = request.form.to_dict(flat=False)
    dl_data = {k.replace("[]", ""): request_dict[k][0] if len(request_dict[k]) <=
               1 and k[-2:] != "[]" else request_dict[k] for k in request_dict}
    try:
        current_dl = DistributionList.query.filter_by(id=id, deleted_at=None).first()
        current_dl.email = dl_data['email']
        current_dl.description = dl_data['description']
        db.session.commit()

        flash(("Distribution list edited", "DL edited successfully"), "success")
        return redirect(url_for('distribution_lists'))
    except Exception as e:
        flash(("Error", e), "error")
        if current_dl is not None:
            dl_data["id"] = current_dl.id
        return render_template_for_dls(dl_data, True)


def post():
    request_dict = request.form.to_dict(flat=False)
    dl_data = {k.replace("[]", ""): request_dict[k][0] if len(
        request_dict[k]) <= 1 else request_dict[k] for k in request_dict}
    try:
        new_dl = DistributionList(email=dl_data['email'], description=dl_data['description'])
        db.session.add(new_dl)
        db.session.commit()

        flash(("Distribution list created", "DL created successfully"), "success")
        return redirect(url_for('distribution_lists'))
    except Exception as e:
        flash(("Error", e), "error")
        return render_template_for_dls(dl_data, False)


def delete(id):
    try:
        DistributionList.query.filter_by(id=id).first().soft_delete()
        db.session.commit()
        flash(("Distribution list deleted", "DL deleted successfully"), "success")
    except Exception as e:
        flash(("Error", str(e)), "error")
    return jsonify({"message": "ok"})


def show_edit(id):
    current_dl = DistributionList.query.filter_by(id=id, deleted_at=None).first()
    return render_template_for_dls(current_dl.__dict__, True)


def show_create():
    return render_template_for_dls({}, edit_flag=False)


def render_template_for_dls(dl_data, edit_flag):
    return render_template(
        'admin/views/new_distribution_list.html',
        dl_data=dl_data,
        title='Distribution List',
        edit_flag=edit_flag)
