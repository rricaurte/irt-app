from flask import redirect, url_for
from scripts import import_csv


def get():
    # import_csv.load_csv_dls('scripts/dls.csv')
    import_csv.load_csv_systems('scripts/systems.csv')
    return redirect(url_for('incidents'))
