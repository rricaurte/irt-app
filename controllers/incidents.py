from flask import jsonify, request, render_template, redirect, url_for, flash, session
from models.incident import *
from models import mailer
from models import pyjira
from models.incident_template import *
from models.irt_system import *
import settings
from threading import Thread
from models.incident_update import *
import datetime


def get():
    Thread(target=warmup_services).start()
    page = int(request.args.get("page")) if request.args.get("page") else 1
    per_page = int(request.args.get("items_per_page")) if request.args.get("items_per_page") else settings.ITEMS_PER_PAGE
    all_incidents = Incident.query.order_by(Incident.id.desc()).paginate(page, per_page=per_page)
    return render_template(
        "admin/views/incidents.html",
        incidents=all_incidents,
        title="Incidents",
        incidents_active=True)


def put(id):
    incident_data = {}
    current_incident = None
    try:
        current_incident = Incident.query.filter_by(id=id).first()
        incident_data = _read_incident_data_from_request()
        current_incident.subject = incident_data["subject"]
        current_incident.description = incident_data["description"]
        current_incident.reporter = incident_data["reporter"]
        current_incident.severity = int(incident_data["severity"]) if incident_data["severity"].isdigit() else -1
        current_incident.systems = incident_data["systems"]
        _load_optional_fields(current_incident, incident_data)

        _save_jira_incident(current_incident)
        if "send_mail" in incident_data:
            _send_incident_email(current_incident)
        db.session.commit()

        flash(("Incident edited", "Incident edited successfully"), "success")
        return redirect(url_for("incidents"))
    except Exception as e:
        flash(("Error", e), "error")
        if current_incident is not None:
            incident_data["id"] = current_incident.id
            incident_data["status"] = current_incident.status
        return _render_template_for_incident(incident_data, True)


def post():
    incident_data = {}
    try:
        incident_data = _read_incident_data_from_request()
        new_incident = Incident(
            subject=incident_data["subject"],
            description=incident_data["description"],
            reporter=incident_data["reporter"],
            code=Incident.INCI_CODE_DEFAULT,
            severity=int(incident_data["severity"]) if incident_data["severity"].isdigit() else -1,
            status=("Closed" if "is_closed" in incident_data else "Open")
        )
        new_incident.systems = incident_data["systems"]
        _load_optional_fields(new_incident, incident_data)
        db.session.add(new_incident)

        _save_jira_incident(new_incident)
        if "send_mail" in incident_data:
            _send_incident_email(new_incident)
        db.session.commit()

        flash(("Incident created", "Incident created successfully"), "success")
        return redirect(url_for("incidents"))
    except Exception as e:
        flash(("Error", e), "error")
        return _render_template_for_incident(incident_data, False)


def show_edit(id):
    Thread(target=warmup_services).start()
    current_incident = Incident.query.filter_by(id=id).first()
    incident_data = current_incident.__dict__
    incident_data["systems"] = current_incident.systems
    return _render_template_for_incident(incident_data, True)


def show_create():
    Thread(target=warmup_services).start()
    return _render_template_for_incident({}, edit_flag=False)


def show_create_from_template(template_id):
    incident_data = {}
    try:
        current_template = IncidentTemplate.query.filter_by(id=template_id, deleted_at=None).first()
        incident_data = current_template.__dict__
        incident_data["template"] = template_id
    except Exception as e:
        print("Error " + str(e))
    return _render_template_for_incident(incident_data, edit_flag=False)


def _read_incident_data_from_request():
    request_dict = request.form.to_dict(flat=False)
    incident_data = {k.replace("[]", ""): request_dict[k][0] if len(request_dict[k])
                     <= 1 and k[-2:] != "[]" else request_dict[k] for k in request_dict}
    if "systems" in incident_data:
        incident_data["systems"] = IrtSystem.query.filter(IrtSystem.id.in_([int(i) for i in incident_data["systems"]])).all()

    return incident_data


def _render_template_for_incident(incident_data, edit_flag):
    all_systems = IrtSystem.query.filter_by(deleted_at=None).all()
    if edit_flag and "systems" in incident_data:
        for sys in incident_data["systems"]:
            if sys.deleted_at is not None:
                all_systems.append(sys)
    all_templates = IncidentTemplate.query.filter_by(deleted_at=None).all()
    incident_updates = []
    if "systems" in incident_data:
        incident_data["systems"] = [sys.__dict__ for sys in incident_data["systems"]]
    if edit_flag and "id" in incident_data:
        incident_updates = Incident.query.filter_by(id=incident_data["id"]).first().get_all_updates()
    if "reporter" not in incident_data:
        incident_data["reporter"] = (session["irt_reporter"] if "irt_reporter" in session else "???")
    return render_template(
        "admin/views/new_incident.html",
        incident_data=incident_data,
        title="Incidents",
        incidents_active=True,
        edit_flag=edit_flag,
        incidents_severities=Incident.ALL_SEVERITIES,
        systems=[sys.__dict__ for sys in all_systems],
        incident_templates=[{"name": t.name, "id": t.id} for t in all_templates],
        incident_updates=[update.__dict__ for update in incident_updates])


def open_or_close_incident(id, status_data):
    try:
        current_incident = Incident.query.filter_by(id=id).first()
        if status_data["new_status"].lower() == "closed":
            current_incident.close_inci()
        _save_jira_incident(current_incident)
        db.session.commit()
        return jsonify({"message": "OK"}), 201
    except Exception as e:
        flash(("Error", "Could not change incident state"), "error")
        return jsonify({"message": "NOT OK"}), 400


def _load_optional_fields(incident, incident_data):
    optional_fields = ["manager", "teams_working", "affected_customers_units", "distributions_lists", "cause"]
    for field in optional_fields:
        setattr(incident, field, incident_data[field] if field in incident_data else "")
    if "creation_datetime" in incident_data:
        incident.creation_datetime = datetime.datetime.strptime(incident_data["creation_datetime"], '%Y/%m/%d %H:%M')
    if "closure_datetime" in incident_data:
        incident.closure_datetime = datetime.datetime.strptime(incident_data["closure_datetime"], '%Y/%m/%d %H:%M')


def _send_incident_email(incident):
    try:
        mailer.send_email(incident)
    except mailer.MailerException as e:
        flash(("Error", str(e)), "error")
    except Exception as e:
        print("Error:" + str(e))
        flash(("Error", "Mailer could not be reached"), "error")


def _save_jira_incident(incident):
    try:
        if not incident.is_on_jira():
            incident.code = pyjira.create_ticket(incident)
        else:
            pyjira.update_ticket(incident)
    except pyjira.JiraException as e:
        flash(("Error", str(e)), "error")
    except Exception as e:
        print("Error:" + str(e))
        flash(("Error", "Jira couldn't be reached"), "error")

def warmup_services():
    try:
        pyjira.is_healthy()
    except Exception as e:
        print("Error:" + str(e))