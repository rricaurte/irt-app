from models.shared import db
from sqlalchemy.orm import validates
from models.incident_update import *
from models.distribution_list import *
import datetime


incident_systems = db.Table(
    "incident_systems",
    db.Column(
        "incident_id",
        db.Integer,
        db.ForeignKey("incident.id"),
        primary_key=True),
    db.Column(
        "system_id",
        db.Integer,
        db.ForeignKey("irt_system.id"),
        primary_key=True))


class Incident(db.Model):
    ALL_SEVERITIES = [sev for sev in range(1, 6)]
    INCI_CODE_DEFAULT = "INCI-XXXX"
    # Compulsory fields
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(120), unique=True, nullable=False)
    subject = db.Column(db.String(80), unique=False, nullable=False)
    creation_datetime = db.Column(db.DateTime, nullable=True)
    closure_datetime = db.Column(db.DateTime, nullable=True)
    severity = db.Column(db.Integer(), unique=False, nullable=False)
    description = db.Column(db.String(1000), unique=False, nullable=False)
    status = db.Column(db.String(120), unique=False, nullable=False)
    reporter = db.Column(db.String(120), unique=False, nullable=False)
    # Optional fields
    cause = db.Column(db.String(120), unique=False, nullable=False)
    teams_working = db.Column(db.String(120), unique=False, nullable=False)
    affected_customers_units = db.Column(db.String(120), unique=False, nullable=False)
    manager = db.Column(db.String(120), unique=False, nullable=False)
    distributions_lists = db.Column(db.String(255), unique=False, nullable=True)

    systems = db.relationship(
        "IrtSystem",
        secondary=incident_systems,
        lazy="subquery",
        backref=db.backref(
            "incident",
            lazy=True))

    def __repr__(self):
        return self.subject

    def get_all_emails(self):
        systems_dls = [dl for sys in self.systems for dl in sys.dls]
        systems_valid_dls = filter(lambda dl: dl.deleted_at is None, systems_dls)
        systems_dls = {dl.email for dl in systems_valid_dls}
        extra_dls = set(self.distributions_lists.replace(" ", "").split(","))
        all_dls = systems_dls.union(extra_dls)
        all_dls.discard("")
        return list(all_dls)

    def get_all_updates(self):
        return IncidentUpdate.query.filter_by(incident_id=self.id)


    def is_on_jira(self):
        return (self.code != self.INCI_CODE_DEFAULT)


    def is_open(self):
        return (self.status.lower() == "open")


    def close_inci(self, closure_datetime=None):
        self.status = "Closed"
        if closure_datetime is not None:
            self.closure_datetime = closure_datetime


    def reopen_inci(self):
        self.status = "Open"

    @validates("code")
    def validate_code(self, key, code):
        if not code or (len(code) == 0) or "INCI-" not in code:
            raise AssertionError("No correct code provided")
        return code

    @validates("subject")
    def validate_subject(self, key, subject):
        if not subject or (len(subject) == 0):
            raise AssertionError("No correct subject provided")
        if len(subject) > 80:
            raise AssertionError("Subject can't have more than 80 characters")
        return subject

    @validates("severity")
    def validate_severity(self, key, severity):
        if not severity or (severity not in self.ALL_SEVERITIES):
            raise AssertionError("No correct severity provided")
        return severity

    @validates("cause")
    def validate_cause(self, key, cause):
        if not cause or (len(cause) == 0):
            return ""
        if len(cause) > 120:
            raise AssertionError("Cause can't have more than 120 characters")
        return cause

    @validates("teams_working")
    def validate_teams_working(self, key, teams_working):
        if not teams_working or (len(teams_working) == 0):
            return ""
        if len(teams_working) > 120:
            raise AssertionError("Teams working can't have more than 120 characters")
        return teams_working

    @validates("description")
    def validate_description(self, key, description):
        if not description or (len(description) == 0):
            raise AssertionError("No description provided")
        if len(description) > 1000:
            raise AssertionError("Description can't have more than 1000 characters")
        return description

    @validates("affected_customers_units")
    def validate_affected_customers_units(self, key, affected_customers_units):
        if not affected_customers_units or (len(affected_customers_units) == 0):
            return ""
        if (len(affected_customers_units) > 120):
            raise AssertionError("Affected customer units field can't have more than 120 characters")
        return affected_customers_units

    @validates("manager")
    def validate_manager(self, key, manager):
        if not manager or (len(manager) == 0):
            return ""
        if (len(manager) > 120):
            raise AssertionError("Manager field can't have more than 120 characters")
        return manager

    @validates("reporter")
    def validate_reporter(self, key, reporter):
        if not reporter or (len(reporter) == 0):
            raise AssertionError("No reporter provided")
        return reporter

    @validates("status")
    def validate_status(self, key, status):
        if not status or (len(status) == 0):
            raise AssertionError("No status provided")
        return status

    @validates("creation_datetime")
    def validate_creation_datetime(self, key, creation_datetime):
        if not creation_datetime:
            raise AssertionError("No starting datetime provided")
        if self.closure_datetime and self.closure_datetime < creation_datetime:
            raise AssertionError("Starting datetime can't be greater than resolution datetime")
        if creation_datetime > datetime.datetime.now():
            raise AssertionError("Incident starting time can't be greater than current time")
        else:
            return creation_datetime

    @validates("closure_datetime")
    def validate_closure_datetime(self, key, closure_datetime):
        if self.creation_datetime and self.creation_datetime > closure_datetime:
            raise AssertionError("Starting datetime can't be greater than resolution datetime")
        if closure_datetime > datetime.datetime.now():
            raise AssertionError("Incident resolution time can't be greater than current time")
        else:
            return closure_datetime
