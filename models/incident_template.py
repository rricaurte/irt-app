from models.shared import db
from sqlalchemy.orm import validates
from models.irt_system import *
from models.distribution_list import *
from datetime import datetime


template_systems = db.Table(
    "template_systems",
    db.Column(
        "template_id",
        db.Integer,
        db.ForeignKey("incident_template.id"),
        primary_key=True),
    db.Column(
        "system_id",
        db.Integer,
        db.ForeignKey("irt_system.id"),
        primary_key=True))


class IncidentTemplate(db.Model):
    ALL_SEVERITIES = [sev for sev in range(1, 6)]
    # Compulsory fields
    id = db.Column(db.Integer, primary_key=True)
    last_updated_at = db.Column(db.DateTime, onupdate=datetime.now, default=datetime.now, nullable=False)
    deleted_at = db.Column(db.DateTime, nullable=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    subject = db.Column(db.String(80), unique=False, nullable=False)
    severity = db.Column(db.Integer(), unique=False, nullable=False)
    description = db.Column(db.String(1000), unique=False, nullable=False)
    # Optional fields
    cause = db.Column(db.String(120), unique=False, nullable=False)
    teams_working = db.Column(db.String(120), unique=False, nullable=False)
    affected_customers_units = db.Column(db.String(120), unique=False, nullable=False)
    manager = db.Column(db.String(120), unique=False, nullable=False)
    distributions_lists = db.Column(db.String(255), unique=False, nullable=True)

    systems = db.relationship(
        "IrtSystem",
        secondary=template_systems,
        lazy="subquery",
        backref=db.backref(
            "incident_template",
            lazy=True))

    def __repr__(self):
        return self.subject

    def get_all_emails(self):
        systems_dls = [dl for sys in self.systems for dl in sys.dls]
        systems_valid_dls = filter(lambda dl: dl.deleted_at is None, systems_dls)
        systems_dls = {dl.email for dl in systems_valid_dls}
        extra_dls = set(self.distributions_lists.replace(" ", "").split(","))
        all_dls = systems_dls.union(extra_dls)
        all_dls.discard("")
        return list(all_dls)

    @validates('name')
    def validate_subject(self, key, name):
        if not name or (len(name) == 0):
            raise AssertionError('No correct template name provided')
        if len(name) > 80:
            raise AssertionError("Template name can't have more than 80 characters")
        repeated_system = IncidentTemplate.query.filter(IncidentTemplate.name == name).first()
        if repeated_system and (repeated_system.id != self.id):
            # If there's another system with the same name and it's not this same system...
            raise AssertionError('Template name is already in use')
        return name

    @validates("subject")
    def validate_subject(self, key, subject):
        if not subject or (len(subject) == 0):
            raise AssertionError("No correct subject provided")
        if len(subject) > 80:
            raise AssertionError("Subject can't have more than 80 characters")
        return subject

    @validates("severity")
    def validate_severity(self, key, severity):
        if not severity or (severity not in self.ALL_SEVERITIES):
            raise AssertionError("No correct severity provided")
        return severity

    @validates("cause")
    def validate_cause(self, key, cause):
        if not cause or (len(cause) == 0):
            return ""
        if len(cause) > 120:
            raise AssertionError("Cause can't have more than 120 characters")
        return cause

    @validates("teams_working")
    def validate_teams_working(self, key, teams_working):
        if not teams_working or (len(teams_working) == 0):
            return ""
        if len(teams_working) > 120:
            raise AssertionError("Teams working can't have more than 120 characters")
        return teams_working

    @validates("description")
    def validate_description(self, key, description):
        if not description or (len(description) == 0):
            raise AssertionError("No description provided")
        if len(description) > 1000:
            raise AssertionError("Description can't have more than 1000 characters")
        return description

    @validates("affected_customers_units")
    def validate_affected_customers_units(self, key, affected_customers_units):
        if not affected_customers_units or (len(affected_customers_units) == 0):
            return ""
        if (len(affected_customers_units) > 120):
            raise AssertionError("Affected customer units field can't have more than 120 characters")
        return affected_customers_units

    @validates("manager")
    def validate_manager(self, key, manager):
        if not manager or (len(manager) == 0):
            return ""
        if (len(manager) > 120):
            raise AssertionError("Manager field can't have more than 120 characters")
        return manager

    @validates("reporter")
    def validate_reporter(self, key, reporter):
        if not reporter or (len(reporter) == 0):
            raise AssertionError("No reporter provided")
        return reporter

    @validates("status")
    def validate_status(self, key, status):
        if not status or (len(status) == 0):
            raise AssertionError("No status provided")
        return status

    def soft_delete(self):
        if self.deleted_at is not None:
            raise AssertionError("Template is already deleted!")
        self.deleted_at = datetime.now()
