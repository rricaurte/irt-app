import os
from flask_sqlalchemy import SQLAlchemy
from flask_sqlalchemy.model import BindMetaMixin, Model
from sqlalchemy.ext.declarative import DeclarativeMeta, declarative_base, declared_attr

db = SQLAlchemy()

TESTING = os.environ.get('TESTING') is not None
if TESTING:
    # Replace base db class with one that doesn't create tables
    class IdModel(Model):
        __table_args__ = {'extend_existing': True}

        @declared_attr
        def id(cls):
            for base in cls.__mro__[1:-1]:
                if getattr(base, '__table__', None) is not None:
                    type = sa.ForeignKey(base.id)
                    break
            else:
                type = sa.Integer

            return sa.Column(type, primary_key=True)

    db = SQLAlchemy(model_class=IdModel)
