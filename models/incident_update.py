from models.shared import db
from sqlalchemy.orm import validates
import models.incident


class IncidentUpdate(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(255), unique=False, nullable=False)
    number = db.Column(db.Integer(), unique=True, nullable=False)
    incident_id = db.Column(db.Integer, db.ForeignKey('incident.id'), nullable=False)

    def __repr__(self):
        return self.content


    @validates('content')
    def validate_content(self, key, content):
        if not content or (len(content) == 0):
            raise AssertionError('No update provided')
        if len(content) > 255:
            raise AssertionError("Update can't have more than 255 characters")
        return content


    @validates('number')
    def validate_number(self, key, number):
        if number < 0:
            raise AssertionError("You can't send negative updates")
        previous_updates = IncidentUpdate.query.filter(IncidentUpdate.incident_id == self.incident_id).all()
        previous_updates_numbers = [updates.number for updates in previous_updates]
        if self.number in previous_updates_numbers:
            raise AssertionError("Wrong update number.")
        return number


    @validates('incident_id')
    def validate_subject(self, key, incident_id):
        updated_incident = models.incident.Incident.query.filter_by(id=incident_id).first()
        if not updated_incident:
            raise AssertionError("The update you are trying to make does not belong to any existing Incident")
        return incident_id
