from flask import render_template
from models.incident import *
from models.incident_update import *
import requests
import settings

def send_email(incident):
    payload = {"source": "irt-no-reply@zappos.com",
               "destinations": incident.get_all_emails(),
               "subject": incident.code +" : " + incident.subject + " - " + incident.status}
    payload["message"] = _render_mail_content(incident)
    res = requests.post(
        settings.MAILER_ENDPOINT,
        json=payload,
        headers=settings.MAILER_HEADERS,
        timeout=settings.MAILER_TIMEOUT)
    print(res.text)
    if res.status_code != 201:
        raise MailerException('Email could not be sent')


def _render_mail_content(incident):
    incident_updates = IncidentUpdate.query.filter_by(incident_id=incident.id)
    incident_updates = [update.__dict__ for update in incident_updates]
    return render_template(
        'admin/views/incident_email.html',
        incident_data=incident.__dict__,
        incident_on_jira=incident.is_on_jira(),
        incident_updates=incident_updates
    )


class MailerException(Exception):
    pass
