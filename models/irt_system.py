from models.shared import db
from sqlalchemy.orm import validates
from models.distribution_list import *
from datetime import datetime
suggested_dls = db.Table(
    'suggested_dl',
    db.Column(
        'system_id',
        db.Integer,
        db.ForeignKey('irt_system.id'),
        primary_key=True),
    db.Column(
        'dl_id',
        db.Integer,
        db.ForeignKey('distribution_list.id'),
        primary_key=True))


class IrtSystem(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    last_updated_at = db.Column(db.DateTime, onupdate=datetime.now, default=datetime.now, nullable=False)
    deleted_at = db.Column(db.DateTime, nullable=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    description = db.Column(db.String(120), unique=False, nullable=False)
    dls = db.relationship(
        'DistributionList',
        secondary=suggested_dls,
        lazy='subquery',
        backref=db.backref(
            'irt_system',
            lazy=True))

    def __repr__(self):
        return self.name

    @validates('name')
    def validate_subject(self, key, name):
        if not name or (len(name) == 0):
            raise AssertionError('No correct system name provided')
        if len(name) > 80:
            raise AssertionError("System name can't have more than 80 characters")
        repeated_system = IrtSystem.query.filter(IrtSystem.name == name).first()
        if repeated_system and (repeated_system.id != self.id):
            # If there's another system with the same name and it's not this same system...
            raise AssertionError('System name is already in use')
        return name

    @validates('description')
    def validate_description(self, key, description):
        if not description or (len(description) == 0):
            raise AssertionError('No description provided')
        if len(description) > 120:
            raise AssertionError("Description can't have more than 120 characters")
        return description

    def soft_delete(self):
        if self.deleted_at is not None:
            raise AssertionError("System is already deleted!")
        self.deleted_at = datetime.now()
        self.name += "- deleted" + str(datetime.now())
