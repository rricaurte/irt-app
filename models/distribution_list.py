from models.shared import db
from sqlalchemy.orm import validates
import re
from datetime import datetime


class DistributionList(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    last_updated_at = db.Column(db.DateTime, onupdate=datetime.now, default=datetime.now, nullable=False)
    deleted_at = db.Column(db.DateTime, nullable=True)
    email = db.Column(db.String(80), unique=True, nullable=False)
    description = db.Column(db.String(120), unique=False, nullable=False)

    def __repr__(self):
        return self.email

    @validates('email')
    def validate_email(self, key, email):
        if not email or (len(email) == 0):
            raise AssertionError('No correct email provided')
        if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
            raise AssertionError('Provided email is not an email address')
        repeated_dl = DistributionList.query.filter(DistributionList.email == email).first()
        if repeated_dl and (repeated_dl.id != self.id):
            # If there's another DL with the same email and it's not this same DL...
            raise AssertionError('DL email is already in use')
        return email

    @validates('description')
    def validate_description(self, key, description):
        if not description or (len(description) == 0):
            raise AssertionError('No description provided')
        if len(description) > 120:
            raise AssertionError("Description can't have more than 120 characters")
        return description

    def soft_delete(self):
        if self.deleted_at is not None:
            raise AssertionError("DL is already deleted!")
        self.deleted_at = datetime.now()
