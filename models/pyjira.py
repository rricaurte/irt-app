import requests
import settings
import json


def create_ticket(incident):
    payload = _inci_to_json(incident)
    payload["closed"] = not incident.is_open()
    res = requests.post(
        settings.PYJIRA_ENDPOINT,
        json=payload,
        headers=settings.PYJIRA_HEADERS,
        timeout=settings.PYJIRA_TIMEOUT)
    print(res.text)
    if res.status_code != 201:
        raise JiraException("Jira incident was not created")
    return json.loads(res.text)["code"]


def update_ticket(incident):
    payload = _inci_to_json(incident)
    payload["closed"] = not incident.is_open()
    inci_code = incident.code.strip("INCI-")
    res = requests.put(
        settings.PYJIRA_ENDPOINT + "/" + inci_code,
        json=payload,
        headers=settings.PYJIRA_HEADERS,
        timeout=settings.PYJIRA_TIMEOUT)
    print(res.text)
    if res.status_code != 200:
        raise JiraException("Jira incident was not updated")


def comment_ticket(inci_code, comment):
    payload = {"comment": comment}
    inci_code = inci_code.strip("INCI-")
    res = requests.post(
        settings.PYJIRA_ENDPOINT + "/" + inci_code + "/comments",
        json=payload,
        headers=settings.PYJIRA_HEADERS,
        timeout=settings.PYJIRA_TIMEOUT)
    print(res.text)
    if res.status_code != 201:
        raise JiraException("Jira incident was not commented")


def is_healthy():
    res = requests.get(
        settings.PYJIRA_ENDPOINT + "/health",
        headers=settings.PYJIRA_HEADERS,
        timeout=settings.PYJIRA_TIMEOUT)
    print(res.text)
    return (res.status_code == 200)


def _inci_to_json(incident):
    incident_description = ""
    if len(incident.cause) > 0:
        incident_description += ("Cause: " + incident.cause + "\n")
    if len(incident.teams_working) > 0:
        incident_description += ("Teams working to resolve: " + incident.teams_working + "\n")
    if len(incident.affected_customers_units) > 0:
        incident_description += ("Affected customer(s)/business unit(s): " + incident.affected_customers_units + "\n")
    if len(incident.manager) > 0:
        incident_description += ("Incident manager: " + incident.manager + "\n")
    if incident.creation_datetime:
        incident_description += ("Starting time: " + str(incident.creation_datetime) + "\n")
    if incident.closure_datetime:
        incident_description += ("Resolution time: " + str(incident.closure_datetime) + "\n")
    incident_description += ("\n" + incident.description)
    return {"subject": incident.subject, "description": incident_description, "severity": incident.severity}


class JiraException(Exception):
    pass