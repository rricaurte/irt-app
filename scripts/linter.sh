python3 -m autopep8 --in-place --aggressive --aggressive --aggressive --max-line-length 140 -v *.py
python3 -m autopep8 --in-place --aggressive --aggressive --aggressive --max-line-length 140 -v models/*.py
python3 -m autopep8 --in-place --aggressive --aggressive --aggressive --max-line-length 140 -v controllers/*.py
python3 -m autopep8 --in-place --aggressive --aggressive --aggressive --max-line-length 140 -v tests/models/*.py
python3 -m autopep8 --in-place --aggressive --aggressive --aggressive --max-line-length 140 -v tests/controllers/*.py
