#!/usr/bin/env python3

from models.distribution_list import *
from models.irt_system import *

import csv

# def load_csv_dls(filename):
#     with open(filename, 'r') as dls_csv:
#         dls = csv.reader(dls_csv, delimiter=',')
#         for dl in dls:
#             post_dl({'email': dl[0], 'description': dl[1]})

def load_csv_systems(filename):
    with open(filename, 'r') as dls_csv:
        systems = csv.reader(dls_csv, delimiter=',')
        count = 0
        for s in systems:
            dls = list(map(str,s[1].replace("\"", "").replace(" ", "").replace("[", "").replace("]", "").split(",")))
            try:
                dls.remove("")
            except:
                pass
            if len(dls) != 0:
                post_system({"name": s[0], "description": "System for "+s[0], "dls": dls})



def post_system(sys_dic):
    system_dls = []
    try:
        for dl in sys_dic["dls"]:
            dl_obj = DistributionList.query.filter(DistributionList.email == dl).first()
            if dl_obj is None:
                print("Adding dl to db")
                try:
                    post_dl({'email': dl, 'description': "DL for " + dl})
                    dl_obj = DistributionList.query.filter(DistributionList.email == dl).first()
                    if dl_obj is None: continue
                    system_dls.append(dl_obj)
                except:
                    pass
        print(system_dls)
        # return
        new_system = IrtSystem(name=sys_dic["name"], description=sys_dic["description"])
        new_system.dls = system_dls
        db.session.add(new_system)
        db.session.commit()
        print("Good!")
    except Exception as e:
        print(e)


def post_dl(dls_dic):
    try:
        new_dl = DistributionList(email=str(dls_dic['email']), description=str(dls_dic['description']))
        db.session.add(new_dl)
        db.session.commit()
        print((dls_dic['email']) + "Loaded!")
        return True
    except Exception as e:
        print(e)
        return False
