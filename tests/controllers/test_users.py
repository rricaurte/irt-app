import pytest
from unittest.mock import patch
from controllers import users
from models.user import *

# A get to the users controller should query all users


@patch('flask_sqlalchemy._QueryProperty.__get__')
@patch('controllers.users.jsonify')
def test_get_users(mock_jsonify, mock_query_all):
    all_users = [User(username="username", email="an@email.com")]
    mock_query_all.return_value.all.return_value = all_users
    users.get()
    mock_query_all.assert_called()
    mock_jsonify.assert_called()
