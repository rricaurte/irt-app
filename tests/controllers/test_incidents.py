import pytest
from unittest.mock import patch
from controllers import incidents
from models.incident import *
from models.distribution_list import *


# A get to show_create should render the new_incident template


@patch('controllers.incidents.flash')
@patch('flask_sqlalchemy._QueryProperty.__get__')
@patch('controllers.incidents.render_template')
def test_show_create(mock_render, mock_query, mock_flash):
    incidents.show_create()
    mock_render.assert_called()
    assert 'admin/views/new_incident.html' in mock_render.call_args[0]


# A get to show_edit should render the new_incident template

@patch('controllers.incidents.flash')
@patch('flask_sqlalchemy._QueryProperty.__get__')
@patch('controllers.incidents.render_template')
def test_show_edit(mock_render, mock_query_one_inci, mock_flash):
    mock_query_one_inci.return_value.filter_by.id = Incident().id
    incidents.show_edit(mock_query_one_inci.return_value.filter_by.id)
    mock_render.assert_called()
    assert 'admin/views/new_incident.html' in mock_render.call_args[0]


# A get to the incidents controller should query all incidents

@patch('controllers.incidents.flash')
@patch('flask_sqlalchemy._QueryProperty.__get__')
@patch('controllers.incidents.render_template')
def test_get_incidents(mock_render, mock_query_all, mock_flash):
    all_incis = [Incident(), Incident()]
    mock_query_all.return_value.all.return_value = all_incis
    incidents.get()
    mock_query_all.assert_called()
    mock_render.assert_called()
    assert 'admin/views/incidents.html' in mock_render.call_args[0]


# Posting a valid incident to the controller should be successful and redirect to another view

@patch('controllers.incidents._read_incident_data_from_request')
@patch('controllers.incidents.mailer.send_email')
@patch('controllers.incidents.request.form')
@patch('controllers.incidents.request')
@patch('controllers.incidents.flash')
@patch('controllers.incidents.db.session')
@patch('controllers.incidents.url_for')
@patch('controllers.incidents._render_template_for_incident')
@patch('controllers.incidents.redirect')
def test_post_valid_incident(
        mock_redirect,
        mock_render,
        mock_url_for,
        mock_db_session,
        mock_flash,
        mock_request,
        mock_form,
        mock_send_email,
        mock_incident_data):
    incident_data = {
        "subject": "Testing incident",
        "description": "This is an incident for this test",
        "reporter": "Pablo Lamponne",
        "systems": [IrtSystem()],
        "code": "INCI-001",
        "severity": 3,
        "type": "Incident",
        "manager": "Mario Santos",
        "teams_working": "The Simulators",
        "affected_customers_units": "The FBI",
        "cause": "The B brigade has fallen on the hands of the Empire",
        "distributions_lists": "",
        "status": 'Open'}
    mock_incident_data.return_value = incident_data
    mock_form.__getitem__.side_effect = incident_data.__getitem__
    incidents.post()
    mock_render.assert_not_called()
    mock_url_for.assert_called()
    mock_send_email.assert_called()
    mock_flash.assert_called()
    mock_redirect.assert_called()
    assert 'success' in mock_flash.call_args[0]


# Posting an invalid incident to the controller should trigger error and redirect to new_incident

@patch('controllers.incidents._read_incident_data_from_request')
@patch('controllers.incidents.request.form')
@patch('controllers.incidents.request')
@patch('controllers.incidents.flash')
@patch('controllers.incidents.db.session')
@patch('controllers.incidents.url_for')
@patch('controllers.incidents._render_template_for_incident')
@patch('controllers.incidents.redirect')
def test_post_invalid_incident(
        mock_redirect,
        mock_render,
        mock_url_for,
        mock_db_session,
        mock_flash,
        mock_request,
        mock_form,
        mock_incident_data):
    incident_data = {
        "subject": "Testing incident",
        "description": "This is an incident for this test",
        "reporter": "Pablo Lamponne",
        "systems": [IrtSystem()],
        "code": "INCI-001",
        "severity": 3,
        "type": "-------AN INVALID TYPE--------",
        "manager": "Mario Santos",
        "teams_working": "The Simulators",
        "affected_customers_units": "The FBI",
        "cause": "The B brigade has fallen on the hands of the Empire",
        "distributions_lists": "",
        "status": 'Open'}
    mock_incident_data.return_value = incident_data
    mock_form.__getitem__.side_effect = incident_data.__getitem__
    incidents.post()
    mock_redirect.assert_not_called()
    mock_render.assert_called()
    mock_url_for.assert_not_called()
    mock_flash.assert_called()
    assert 'error' in mock_flash.call_args[0]
