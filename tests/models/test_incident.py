import pytest
from models.incident import *

# An incident with all valid fields should be valid


def test_valid_incident():
    inci = Incident(
        subject="Testing incident",
        description="This is an incident for this test",
        reporter="Pablo Lamponne",
        code="INCI-001",
        severity=3,
        type="Incident",
        manager="Mario Santos",
        teams_working="The Simulators",
        affected_customers_units="The FBI",
        cause="The B brigade has fallen on the hands of the Empire",
        status='Open')
    assert inci.code == "INCI-001"
    assert inci.subject == "Testing incident"
    assert inci.type == "Incident"

# An incident with an empty subject should be invalid


def test_empty_subject():
    with pytest.raises(Exception):
        inci = Incident(
            subject="",
            description="This is an incident for this test",
            reporter="Pablo Lamponne",
            code="INCI-001",
            severity=3,
            type="Incident",
            manager="Mario Santos",
            teams_working="The Simulators",
            affected_customers_units="The FBI",
            cause="The B brigade has fallen on the hands of the Empire",
            status='Open')

# An incident with an empty description should be invalid


def test_empty_description():
    with pytest.raises(Exception):
        inci = Incident(
            subject="Testing incident",
            description="",
            reporter="Pablo Lamponne",
            code="INCI-001",
            severity=3,
            type="Incident",
            manager="Mario Santos",
            teams_working="The Simulators",
            affected_customers_units="The FBI",
            cause="The B brigade has fallen on the hands of the Empire",
            status='Open')

# An incident with an empty reporter should be invalid


def test_empty_reporter():
    with pytest.raises(Exception):
        inci = Incident(
            subject="Testing incident",
            description="This is an incident for this test",
            reporter="",
            code="INCI-001",
            severity=3,
            type="Incident",
            manager="Mario Santos",
            teams_working="The Simulators",
            affected_customers_units="The FBI",
            cause="The B brigade has fallen on the hands of the Empire",
            status='Open')

# An incident with an empty code should be invalid


def test_empty_code():
    with pytest.raises(Exception):
        inci = Incident(
            subject="Testing incident",
            description="This is an incident for this test",
            reporter="Pablo Lamponne",
            code="",
            severity=3,
            type="Incident",
            manager="Mario Santos",
            teams_working="The Simulators",
            affected_customers_units="The FBI",
            cause="The B brigade has fallen on the hands of the Empire",
            status='Open')

# An incident with an empty type should be invalid


def test_empty_type():
    with pytest.raises(Exception):
        inci = Incident(
            subject="Testing incident",
            description="This is an incident for this test",
            reporter="Pablo Lamponne",
            code="INCI-001",
            severity=3,
            type="",
            manager="Mario Santos",
            teams_working="The Simulators",
            affected_customers_units="The FBI",
            cause="The B brigade has fallen on the hands of the Empire",
            status='Open')

# An incident with an empty manager should be invalid


def test_empty_manager():
    with pytest.raises(Exception):
        inci = Incident(
            subject="Testing incident",
            description="This is an incident for this test",
            reporter="Pablo Lamponne",
            code="INCI-001",
            severity=3,
            type="Incident",
            manager="",
            teams_working="The Simulators",
            affected_customers_units="The FBI",
            cause="The B brigade has fallen on the hands of the Empire",
            status='Open')

# An incident with no teams working should be invalid


def test_empty_teams():
    with pytest.raises(Exception):
        inci = Incident(
            subject="Testing incident",
            description="This is an incident for this test",
            reporter="Pablo Lamponne",
            code="INCI-001",
            severity=3,
            type="Incident",
            manager="Mario Santos",
            teams_working="",
            affected_customers_units="The FBI",
            cause="The B brigade has fallen on the hands of the Empire",
            status='Open')

# An incident with no affected customer units should be invalid


def test_empty_affected_units():
    with pytest.raises(Exception):
        inci = Incident(
            subject="Testing incident",
            description="This is an incident for this test",
            reporter="Pablo Lamponne",
            code="INCI-001",
            severity=3,
            type="Incident",
            manager="Mario Santos",
            teams_working="The Simulators",
            affected_customers_units="",
            cause="The B brigade has fallen on the hands of the Empire",
            status='Open')

# An incident with an empty cause should be invalid


def test_empty_cause():
    with pytest.raises(Exception):
        inci = Incident(
            subject="Testing incident",
            description="This is an incident for this test",
            reporter="Pablo Lamponne",
            code="INCI-001",
            severity=3,
            type="Incident",
            manager="Mario Santos",
            teams_working="The Simulators",
            affected_customers_units="The FBI",
            cause="",
            status='Open')


# An incident with a long subject should be invalid


def test_long_subject():
    with pytest.raises(Exception):
        inci = Incident(
            subject="Lorem ipsum dolor sit amet consectetur adipiscing elit lacinia, gravida vestibul.",
            description="This is an incident for this test",
            reporter="Pablo Lamponne",
            code="INCI-001",
            severity=3,
            type="Incident",
            manager="Mario Santos",
            teams_working="The Simulators",
            affected_customers_units="The FBI",
            cause="The B brigade has fallen on the hands of the Empire",
            status='Open')


# An incident with a long description should be invalid


def test_long_description():
    with pytest.raises(Exception):
        inci = Incident(
            subject="Testing incident",
            description="Lorem ipsum dolor sit amet consectetur adipiscing elit himenaeos ut cursus pellentesque, luctus habitant augue hendrerit sapien quam risus vulputate curabitur euismod. Morbi tempor eleifend tempus bibendum maecenas malesuada facilisis, eget ultricies dapibus dis purus praesent diam, quam mauris pharetra accumsan condimentum blandit. Fames quis ad accumsan suscipit felis donec, porta massa nascetur sociis imperdiet ul.",
            reporter="Pablo Lamponne",
            code="INCI-001",
            severity=3,
            type="Incident",
            manager="Mario Santos",
            teams_working="The Simulators",
            affected_customers_units="The FBI",
            cause="The B brigade has fallen on the hands of the Empire",
            status='Open')


# An incident with no Type should be invalid


def test_empty_status():
    with pytest.raises(Exception):
        inci = Incident(
            subject="Testing incident",
            description="Lorem ipsum dolor sit amet consectetur adipiscing elit himenaeos ut cursus pellentesque, luctus habitant augue hendrerit sapien quam risus vulputate curabitur euismod. Morbi tempor eleifend tempus bibendum maecenas malesuada facilisis, eget ultricies dapibus dis purus praesent diam, quam mauris pharetra accumsan condimentum blandit. Fames quis ad accumsan suscipit felis donec, porta massa nascetur sociis imperdiet ul.",
            reporter="Pablo Lamponne",
            code="INCI-001",
            severity=3,
            type="Incident",
            manager="Mario Santos",
            teams_working="The Simulators",
            affected_customers_units="The FBI",
            cause="The B brigade has fallen on the hands of the Empire",
            status='')

# An incident with an empty type should be invalid


def test_wrong_tpye():
    with pytest.raises(Exception):
        inci = Incident(
            subject="Testing incident",
            description="This is an incident for this test",
            reporter="Pablo Lamponne",
            code="INCI-001",
            severity=3,
            type="Otherthingy",
            manager="Mario Santos",
            teams_working="The Simulators",
            affected_customers_units="The FBI",
            cause="The B brigade has fallen on the hands of the Empire",
            status='Open')
