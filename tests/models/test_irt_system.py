import pytest
from unittest.mock import patch
from models.irt_system import *
from models.distribution_list import *

# A system with all valid fields should be valid


@patch('flask_sqlalchemy._QueryProperty.__get__')
def test_valid_system(mock_search):
    mock_search.return_value.filter.return_value.first.return_value = []
    system = IrtSystem(
        name="6pm",
        description="System for 6pm"
    )
    assert system.name == "6pm"
    assert system.description == "System for 6pm"

# An incident with an empty name should be invalid


@patch('flask_sqlalchemy._QueryProperty.__get__')
def test_empty_name(mock_search):
    mock_search.return_value.filter.return_value.first.return_value = []
    with pytest.raises(Exception):
        system = IrtSystem(
            name="",
            description="System for 6pm"
        )

# An incident with an empty description should be invalid


@patch('flask_sqlalchemy._QueryProperty.__get__')
def test_empty_description(mock_search):
    mock_search.return_value.filter.return_value.first.return_value = []
    with pytest.raises(Exception):
        system = IrtSystem(
            name="6pm",
            description=""
        )

# An incident with a repeated name should be invalid


@patch('flask_sqlalchemy._QueryProperty.__get__')
def test_repeated_name(mock_search):
    mock_search.return_value.filter.return_value.first.return_value = [IrtSystem()]
    with pytest.raises(Exception):
        system = IrtSystem(
            name="6pm",
            description="System for 6pm"
        )

# An incident with a long name should be invalid


@patch('flask_sqlalchemy._QueryProperty.__get__')
def test_long_name(mock_search):
    mock_search.return_value.filter.return_value.first.return_value = [IrtSystem()]
    with pytest.raises(Exception):
        system = IrtSystem(
            name="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin rutrum massa nunc.",
            description="System for lorem ipsum"
        )
