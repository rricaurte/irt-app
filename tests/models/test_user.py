import pytest
from models.user import *

# A user with a valid username and email should be valid


def test_valid_user():
    u = User(username="somename", email="some@email.com")
    assert u.username == "somename"
    assert u.email == "some@email.com"

# A user with an invalid username should raise exception


def test_invalid_username():
    with pytest.raises(Exception):
        u = User(username="_", email="some@email.com")

# A user with an invalid email should raise exception


def test_invalid_email():
    with pytest.raises(Exception):
        u = User(username="somename", email="some-email.com")
