# IRT-app

## Installation

The installation requires npm and pip3+virtualenv:

```
$ virtualenv --python=python3.6 venv
$ sudo npm install -g serverless
$ sudo npm install -n serverless-python-requirements serverless-wsgi serverless-s3-deploy
$ source venv/bin/activate
$ pip3 install -r requirements.txt
```

For more details on how to install npm, pip3, virtualenv or anything needed, please refer to the `docs/project_dependencies.md` file.

## Local development

### Pre-requisites

For local developing, you must have `mysql` installed and an user `'mysql'@'localhost'` with password `"mysql"` and a database `"irt"`.
To do so, once mysql/mariadb is installed:

```mysql
CREATE DATABASE irt;
CREATE USER 'mysql'@'localhost' IDENTIFIED BY 'mysql';
GRANT ALL PRIVILEGES ON irt.* TO 'mysql'@'localhost';
FLUSH PRIVILEGES;
```

### Migrations

To run the migrations locally, activate the virtual environment and run:

```bash
(venv) $ bash ./scripts/migrations.sh
```

### Launching the app locally

To test the app locally, activate the virtual environment and run:

```bash
(venv) $ sls wsgi serve
 * Running on http://localhost:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
```

Navigate to [localhost:5000](http://localhost:5000) to see your app running locally.

## Developing workflow

The flow for developing a feature and deploying it to production is the following:

* First, an issue must be created for that feature, with the _To Do_ label. All other proper labels should be set as well. 
* When someone wants to start working on that feature, they must create a new branch from _dev_ with the following name: [issue number]_[issue name in snake_case]. Example: 00_walking_skeleton. The respective issue is then updated to have the *Work in progress* label.
* The development of the feature must remain contained on the feature branch until the feature is finished.
* Once the development is finished, a merge request against _dev_ branch must be opened and someone else should perform a code review. The issue label is changed then to _In review_. 
* If the reviewer points out something that should be corrected, the merge request is left open and changes are pushed to the same branch. Only after a successful review the feature branch should be merged.
* After the feature branch is merged into _dev_, the original issue is marked as closed.
* At the end of each iteration, all features are shown to the product owner. Only if the product owner validates all work done, the features should be merged into _master_.  

## Deployment on AWS

Use serverless framework to deploy to AWS (you must have first configured aws cli with your proper credentials):

```
$ sls deploy
Serverless: Parsing Python requirements.txt
Serverless: Installing required Python packages for runtime python3.6...
Serverless: Docker Image: lambci/lambda:build-python3.6
..............
Serverless: Stack update finished...
Service Information
service: irt-app
stage: dev
region: us-east-1
stack: irt-app-dev
api keys:
  None
endpoints:
  ANY - https://qbdd5v4b1l.execute-api.us-east-1.amazonaws.com/dev
  ANY - https://qbdd5v4b1l.execute-api.us-east-1.amazonaws.com/dev/{proxy+}
  (...)
functions:
  app: irt-app-dev-app
layers:
  None
```

Navigate to the given endpoint (https://qbdd5v4b1l.execute-api.us-east-1.amazonaws.com/dev in this example) to see the app deployed on AWS.
