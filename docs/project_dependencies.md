# IRT project dependencies

The project uses the following stuff:

- Python3.6 as the project language, with the Flask framework.
- pip3 and virtualenv for handling Python dependencies.
- MySQL (or MariaDB) as the relational database.
- serverless for deploying the server as a Lambda function with and RDS instance.
- npm for handling serverless plugins. 
- Docker for compiling the Python dependecies as on the AWS Lambda environment when uploading them with serverless.  

For installing everything in Ubuntu just follow this commands:

```bash
sudo apt-get install python3-pip nodejs npm
sudo pip3 install awscli --upgrade --user
sudo pip3 install virtualenv
sudo npm install -g serverless serverless-python-requirements serverless-wsgi serverless-s3-deploy
sudo apt-get install mysql-server mysql-client libmysqlclient-dev
virtualenv --python=python3.6 venv
source venv/bin/activate
pip3 install -r requirements.txt
sudo apt-get install apt-transport-https ca-certificates gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu /
$(lsb_release -cs) /
stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

For installing  everything in Mac:

```bash
brew install python3
brew postinstall python3
sudo pip3 install awscli --upgrade --user
sudo pip3 install virtualenv
sudo npm install -g serverless 
sudo npm install -g serverless-python-requirements serverless-wsgi serverless-s3-deploy
brew install mysql
virtualenv --python=python3.6 venv
pip3 install -r requirements.txt
```
