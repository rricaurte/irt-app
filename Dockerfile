# AWS Lambda execution environment is based on Amazon Linux 1
FROM lambci/lambda:build-python3.6

# Install your dependencies
RUN echo "Installing MYSQL" && yum -y install python3-devel python3.6-dev libmysqlclient-dev mysql-devel gcc

# Set the same WORKDIR as default image
WORKDIR /var/task
