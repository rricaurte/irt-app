import os

# TODO: Move all of these as ENV variables on serverless

IS_OFFLINE = os.environ.get('LAMBDA_TASK_ROOT') is None
IRT_ENVIRONMENT = os.environ.get('IRT_ENVIRONMENT')
ITEMS_PER_PAGE = 100

MAILER_API_KEY = os.environ.get("MAILER_API_KEY")
MAILER_APIGW_ID = os.environ.get("MAILER_APIGW_ID")
MAILER_ENDPOINT = os.environ.get("MAILER_ENDPOINT")
MAILER_HEADERS = {"x-api-key": MAILER_API_KEY, "x-apigw-api-id": MAILER_APIGW_ID}
MAILER_TIMEOUT = 8  # In seconds

PYJIRA_API_KEY = os.environ.get("PYJIRA_API_KEY")
PYJIRA_APIGW_ID = os.environ.get("PYJIRA_APIGW_ID")
PYJIRA_ENDPOINT = os.environ.get("PYJIRA_ENDPOINT")
PYJIRA_HEADERS = {"x-api-key": PYJIRA_API_KEY, "x-apigw-api-id": PYJIRA_APIGW_ID}
PYJIRA_TIMEOUT = 19  # In seconds

LDAP_API_KEY = os.environ.get("LDAP_API_KEY")
LDAP_APIGW_ID = os.environ.get("LDAP_APIGW_ID")
LDAP_ENDPOINT = os.environ.get("LDAP_ENDPOINT")
LDAP_HEADERS = {"x-api-key": LDAP_API_KEY, "x-apigw-api-id": LDAP_APIGW_ID}
LDAP_TIMEOUT = 16  # In seconds

SESSION_TIMEOUT = 3500  # In seconds

JIRA_URL = os.environ.get("JIRA_URL")

if IS_OFFLINE:
    # Not in Lambda environment
    DB_URL = "mysql://mysql:mysql@localhost/irt"
    STATIC_URL = "http://localhost:5000/templates/"
    IRT_ENVIRONMENT = 'http://localhost:5000'
    BYPASS_LOGIN = True
else:
    # Inside Lambda environment
    DB_URL = "mysql://" + os.environ.get("IRT_DBUSER") + ":" + os.environ.get("IRT_DBPASS") + \
        "@" + os.environ.get("DATABASE_RDS_ENDPOINT") + "/" + os.environ.get("IRT_DBNAME")
    # print("DEBUG RDS ENDPOINT: " + DB_URL)
    STATIC_URL = "https://" + os.environ.get('STATIC_FILES_BUCKET') + ".s3-us-west-2.amazonaws.com/"
    JIRA_URL = "https://jira.zappos.net"
    # JIRA_URL = "https://jebra.zappos.net"
    BYPASS_LOGIN = False
    # print("DEBUG S# EDNPOINT: " + STATIC_URL)

# Uncomment and adapt the following line to run migrations on production
# DB_URL = "mysql://irtdbuser:irtdbpass@irvli257r54w6i.c3qcbzkkaaux.us-west-2.rds.amazonaws.com:3302/irtdb"
