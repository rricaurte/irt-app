from functools import wraps
from flask import Flask, redirect, render_template, url_for, request, session
from flask_sqlalchemy import SQLAlchemy
from models.shared import db

import settings
import json
import controllers
import os

app = Flask(__name__, static_folder='templates')
app.config['SQLALCHEMY_DATABASE_URI'] = settings.DB_URL
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SECRET_KEY'] = os.environ.get("SECRET_KEY")
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['PERMANENT_SESSION_LIFETIME'] = settings.SESSION_TIMEOUT

db = SQLAlchemy(app)


def login_required(function_to_protect):
    @wraps(function_to_protect)
    def wrapper(*args, **kwargs):
        if ("irt_reporter" in session) or settings.BYPASS_LOGIN:  # Success!
            return function_to_protect(*args, **kwargs)
        else:
            return redirect(url_for('login'))
    return wrapper


@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return render_template('admin/404.html'), 404


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        return controllers.authentication.login()
    else:
        return controllers.authentication.show_login()


@app.route('/logout', methods=['GET'])
def logout():
    return controllers.authentication.logout()


@app.route("/", methods=['GET'])
def homepage():
    return controllers.home.get()


@app.route('/incidents', methods=['GET', 'POST'])
@login_required
def incidents():
    if request.method == 'POST':
        return controllers.incidents.post()
    else:
        return controllers.incidents.get()


@app.route('/incidents/<id>', methods=['GET', 'POST'])
@login_required
def incidents_id(id):
    if request.method == 'POST':
        try:
            if request.data is not None and "new_status" in json.loads(request.data):
                return controllers.incidents.open_or_close_incident(id, json.loads(request.data))
            else:
                return controllers.incidents.put(id)
        except BaseException:
            return controllers.incidents.put(id)
    else:
        return controllers.incidents.show_edit(id)


@app.route('/new_incident', methods=['GET'])
@login_required
def new_incident():
    return controllers.incidents.show_create()


@app.route('/systems', methods=['GET', 'POST'])
@login_required
def systems():
    if request.method == 'POST':
        return controllers.irt_systems.post()
    else:
        return controllers.irt_systems.get()


@app.route('/systems/<id>', methods=['GET', 'POST', 'DELETE'])
@login_required
def system_id(id):
    if request.method == 'POST':
        return controllers.irt_systems.put(id)
    elif request.method == 'DELETE':
        return controllers.irt_systems.delete(id)
    else:
        return controllers.irt_systems.show_edit(id)


@app.route('/systems/dls/<id>', methods=['GET'])
@login_required
def system_dls_id(id):
    if request.method == 'GET':
        return controllers.irt_systems.get_dls(id)


@app.route('/new_system', methods=['GET'])
@login_required
def new_system():
    return controllers.irt_systems.show_create()


@app.route('/distribution_lists', methods=['GET', 'POST'])
@login_required
def distribution_lists():
    if request.method == 'POST':
        return controllers.distribution_lists.post()
    else:
        return controllers.distribution_lists.get()


@app.route('/distribution_lists/<id>', methods=['GET', 'POST', 'DELETE'])
@login_required
def distribution_list_id(id):
    if request.method == 'POST':
        return controllers.distribution_lists.put(id)
    elif request.method == 'DELETE':
        return controllers.distribution_lists.delete(id)
    else:
        return controllers.distribution_lists.show_edit(id)


@app.route('/new_distribution_lists', methods=['GET'])
@login_required
def new_distribution_list():
    return controllers.distribution_lists.show_create()


@app.route('/templates', methods=['GET', 'POST'])
@login_required
def templates():
    if request.method == 'POST':
        return controllers.incident_templates.post()
    else:
        return controllers.incident_templates.get()


@app.route('/templates/<id>', methods=['GET', 'POST', 'DELETE'])
@login_required
def template_id(id):
    if request.method == 'POST':
        return controllers.incident_templates.put(id)
    elif request.method == 'DELETE':
        return controllers.incident_templates.delete(id)
    else:
        return controllers.incident_templates.show_edit(id)


@app.route('/templates/<id>/new_incident', methods=['GET'])
@login_required
def new_incident_from_template(id):
    return controllers.incidents.show_create_from_template(id)


@app.route('/new_template', methods=['GET'])
@login_required
def new_template():
    return controllers.incident_templates.show_create()


@app.route('/users', methods=['GET', 'POST'])
def users():
    if request.method == 'POST':
        return controllers.users.post()
    else:
        return controllers.users.get()


@app.route('/incidents/<inci_id>/updates', methods=['GET', 'POST'])
@login_required
def updates(inci_id):
    if request.method == 'POST':
        return controllers.incident_updates.post(inci_id)
    else:
        return controllers.incident_updates.get(inci_id)


@app.route('/incidents/<inci_id>/current_state', methods=['POST'])
@login_required
def current_state(inci_id):
    return controllers.incident_updates.current_state(inci_id)


@app.context_processor
def inject_static_url():
    return dict(
        static_url=settings.STATIC_URL,
        irt_env=settings.IRT_ENVIRONMENT,
        jira_url=settings.JIRA_URL
    )



if __name__ == "__main__":
    app.run()
